<?php
/**
 * Created by PhpStorm.
 * User: zmajdy
 * Date: 10/7/2016
 * Time: 3:54 PM
 */
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserToken
 * @package App
 *
 * @property  string token
 * @property  date expire_at
 * @property  string ip_address
 */
class UserToken extends Model
{
    const TOKEN_DURATION = 365*24*60*60;

    public function __construct(array $attributes = [])
    {
        $this->expire_at =   date('Y-m-d H:i:s', time() + self::TOKEN_DURATION);
        parent::__construct($attributes);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'token', 'expire_at','ip_address','user_id'
    ];

    public function user() {
        return $this->belongsTo('App\User','user_id','id');
    }
}
