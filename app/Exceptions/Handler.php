<?php

namespace App\Exceptions;

use Exception;
use HttpException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use InvalidArgumentException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Process\Exception\RuntimeException;

class Handler extends ExceptionHandler
{
    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof ValidationException) {
            $data = ['form-validations' => $e->validator->errors()->all(), 'exception' => $e->getMessage()];
            return $this->clientErrorResponse($data);
        }
        elseif ($e instanceof ModelNotFoundException) {
            return $this->notFoundResponse($e->getMessage());
        }
        elseif ($e instanceof NotFoundHttpException) {
            return $this->notFoundResponse($e->getMessage());
        }
        elseif ($e instanceof InvalidArgumentException) {
            return $this->basicErrorResponse('Invalid argument');
        }
        elseif ($e instanceof HttpException) {
            return $this->basicErrorResponse(Response::$statusTexts[$e->getCode()], $e->getCode());
        }
        elseif ($e instanceof RuntimeException) {
            return $this->basicErrorResponse($e->getMessage());
        }
        elseif ($e instanceof MethodNotAllowedHttpException) {
            return $this->basicErrorResponse(Response::$statusTexts[$e->getStatusCode()], $e->getStatusCode());
        }
        elseif ($e instanceof AuthenticationException) {
            return $this->basicErrorResponse($e->getMessage(), '401');
        }
        return parent::render($request, $e);
    }


    protected function notFoundResponse($message)
    {
        $response = [
            'code' => 404,
            'status' => 'error',
            'data' => $message,
            'message' => 'Resource Not Found'
        ];
        return response()->json($response, $response['code']);
    }

    protected function clientErrorResponse($data)
    {
        $response = [
            'code' => 422,
            'status' => 'error',
            'data' => $data,
            'message' => 'Unprocessable entity'
        ];
        return response()->json($response, $response['code']);
    }

    protected function basicErrorResponse($message,$code=422)
    {
        $response = [
            'code' => $code,
            'status' => 'error',
            'message' => $message
        ];
        return response()->json($response, $response['code']);
    }
}
