<?php
/**
 * Created by PhpStorm.
 * User: zmajdy
 * Date: 10/7/2016
 * Time: 4:04 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Order
 * @package App
 * @property string status
 * @property string customer_name
 * @property string email
 * @property string phone
 * @property string payment_proof
 * @property integer user_id
 * @property integer shipping_id
 * @property integer coupon_id
 */
class Order extends Model
{
    const STATUS_SHIPPED = "shipped";
    const STATUS_CONFIRMED = "confirmed";
    const STATUS_PENDING = "pending";
    const STATUS_CANCELED = "canceled";
    const STATUS_ACTIVE = "active";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_name',
        'email',
        'phone',
        'payment_proof',
        'user_id',
        'coupon_id',
        'shipping_id'
    ];

    protected $appends = ['item_list'];

    protected $hidden = ['items', 'coupon'];

    public function __construct(array $attributes = [])
    {
        $this->status = self::STATUS_ACTIVE;
        parent::__construct($attributes);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function coupon() {
        return $this->belongsTo('App\Coupon', 'coupon_id', 'uuid');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function shipping() {
        return $this->belongsTo('App\Shipping', 'shipping_id', 'uuid');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user() {
        return $this->belongsTo('App\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function items()
    {
        return $this->belongsToMany('App\Product')->withPivot('quantity')->select(['id', 'name', 'price']);
    }

    /**
     * @return array
     */
    public function getItemListAttribute()
    {
        $result = ['total_price' => 0, 'items' => []];

        foreach ($this->items as $item) {
            $result['items'][] = [
                'item' => ['id' => $item['id'], 'name' => $item['name'], 'price' => $item['price']],
                'quantity' => $item->pivot->quantity,
                'total' => $item->pivot->quantity * $item->price
            ];
            $result['total_price'] += $item->pivot->quantity * $item->price;
        }

        if ($this->coupon) {
            $result['discount'] = $this->coupon->getDiscountValue($result['total_price']);
            $result['actual_price'] = $result['total_price'];
            $result['total_price'] = $result['total_price'] - $result['discount'];
        }

        return $result;
    }

    /**
     * @param $user_id
     * @return Order
     */
    public static function getLastOrder($user_id)
    {
        return Order::firstOrCreate(['user_id' => $user_id, 'status' => self::STATUS_ACTIVE]);
    }
}