<?php
/**
 * Created by PhpStorm.
 * User: zmajdy
 * Date: 10/7/2016
 * Time: 3:59 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Coupon
 * @property string  uuid
 * @property string  name
 * @property integer quantity
 * @property integer discount
 * @property string discount_type
 * @property integer start_valid_at
 * @property integer end_valid_at
 */
class Coupon extends Model
{
    const PERCENT_TYPE_COUPON = "percent";
    const NOMINAL_TYPE_COUPON = "nominal";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'quantity',
        'discount',
        'discount_type',
        'start_valid_at',
        'end_valid_at'
    ];

    function __construct($attributes = array())
    {
        parent::__construct($attributes);
        $this->uuid = uniqid();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function order()
    {
        return $this->hasOne('App\Order', 'coupon_id', 'uuid');
    }

    public function setStartValidAtAttribute($value) {
        $this->attributes['start_valid_at'] =  date('Y-m-d H:i:s',$value);
    }

    public function setEndValidAtAttribute($value) {
        $this->attributes['end_valid_at'] =  date('Y-m-d H:i:s',$value);
    }

    public function getDiscountValue($price)
    {
        if ($this->discount_type == Coupon::NOMINAL_TYPE_COUPON) {
            $discount = $this->discount;
        } elseif ($this->discount_type == Coupon::PERCENT_TYPE_COUPON) {
            $discount = $this->discount * $price / 100;
        } else $discount = 0;

        return min($discount, $price);
    }
}

