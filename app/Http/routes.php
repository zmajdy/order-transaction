<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use App\User;
use Laravel\Lumen\Application;

function rest($path, $controller, $auth = [], $prefix='/api/v1', $namespace = 'App\Http\Controllers')
{
    global $app;
    $app->group(['prefix' => $prefix, 'namespace' => $namespace, 'middleware' => $auth], function (Application $app) use ($path, $controller) {
        $app->get($path, $controller . '@index');
        $app->get($path . '/{id}', $controller . '@show');
        $app->post($path, $controller . '@store');
        $app->put($path . '/{id}', $controller . '@update');
        $app->delete($path . '/{id}', $controller . '@destroy');
    });
}

$app->get('/', function () use ($app) {
    return $app->version();
});

$auth_admin = 'auth:'.User::USER_ADMIN_ROLE;
$auth_customer = 'auth:'.User::USER_CUSTOMER_ROLE;

rest('products', 'ProductController',$auth_customer);
rest('coupons', 'CouponController',$auth_admin);
rest('users', 'UserController',$auth_admin);
rest('orders', 'OrderController',$auth_admin);
rest('cart', 'UserCartController', $auth_customer, '/api/v1/users/{userid}');
rest('orders', 'UserOrderController', $auth_customer, '/api/v1/users/{userid}');
rest('tokens', 'UserTokenController', [], '/api/v1/users/{userid}');