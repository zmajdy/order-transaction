<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Auth\Factory as Auth;

class Authenticate
{
    /**
     * The authentication guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }


    /**
     * Handle incoming request
     * @param $request
     * @param Closure $next
     * @param string $role
     * @return mixed
     * @throws AuthenticationException
     */
    public function handle($request, Closure $next, $role = User::USER_CUSTOMER_ROLE)
    {
        $user = \Auth::user();

        if ($user->role == $role || $user->role == User::USER_ADMIN_ROLE) {
            return $next($request);
        }
        else {
            throw new AuthenticationException("Unauthorized access to resource");
        }
    }
}
