<?php
/**
 * Created by PhpStorm.
 * User: zmajdy
 * Date: 10/9/2016
 * Time: 11:16 PM
 */

namespace App\Http\Controllers;


use App\User;
use App\UserToken;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserTokenController extends RestController
{

    /**
     * get model namespace
     * @return string
     */
    public function getModel()
    {
        return 'App\User';
    }

    /**
     * get validation rules
     * @return array
     */
    public function getValidationRules()
    {
        return [
            'email' => 'email',
            'password' => 'string'
        ];
    }

    public function index(Request $request, ...$id)
    {
        $user = User::findOrFail($id[0]);
        $this->validateUser($user->id);

        return $this->listResponse($user->tokens);
    }

    public function destroy(...$id)
    {
        $user = User::findOrFail($id[1]);
        $this->validateUser($user->id);

        $user->tokens->find($id[0])->delete();
        return $this->deletedResponse();
    }

    public function show(...$id)
    {
        $user = User::findOrFail($id[1]);
        $this->validateUser($user->id);

        return $this->showResponse($user->tokens->find($id[0]));
    }

    public function store(Request $request, ...$id)
    {
        $this->validate($request,$this->getStoreValidationRules());

        $user = User::whereEmail($request->get('email'))->firstOrFail();
        $pass = $request->get('password');

        if (!password_verify($pass,$user->password)) {
            throw new NotFoundHttpException("invalid email or password");
        }

        $token = UserToken::create([
            'ip_address' => $request->ip(),
            'token' => bin2hex(openssl_random_pseudo_bytes(16)),
            'user_id' => $id[0]
        ]);


        return $this->showResponse($token)->header("Authorization","Bearer ".$token->token);
    }
}