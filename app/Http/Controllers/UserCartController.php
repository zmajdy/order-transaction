<?php
/**
 * Created by PhpStorm.
 * User: zmajdy
 * Date: 10/8/2016
 * Time: 4:42 PM
 */

namespace App\Http\Controllers;


use App\Order;
use App\Product;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\Process\Exception\RuntimeException;

class UserCartController extends RestController
{
    /**
     * get model namespace
     * @return string
     */
    public function getModel()
    {
        return 'App\Product';
    }

    /**
     * get validation rules
     * @return array
     */
    public function getValidationRules()
    {
        return [
            'product_id' => 'integer',
            'quantity'   => 'integer|min:0',
        ];
    }

    /**
     * Store item on cart
     * @param Request $request
     * @param array ...$id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, ...$id)
    {
        $user_id = $id[0];
        $this->validate($request, $this->getStoreValidationRules());
        $this->validateUser($user_id);

        $order    = Order::getLastOrder($user_id);
        $product  = Product::findOrFail($request->get('product_id'));
        $quantity = $request->get('quantity');

        if ($order->items->contains($product)) {
            $quantity += $order->items->find($product)->pivot->quantity;
            $order->items()->detach($product);
        }

        if ($product->quantity < $quantity) {
            throw new RuntimeException("Insufficient product quantity");
        }

        $order->items()->attach($product,['quantity'=>$quantity]);
        $product->quantity -= $quantity;
        $product->save();

        return parent::createdResponse(Order::getLastOrder($user_id)->item_list);
    }

    /**
     * Get Order Detail from specific user (cart list)
     * @param Request $request
     * @param array ...$id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request,...$id)
    {
        $user_id = $id[0];
        $this->validateUser($user_id);

        $order = Order::getLastOrder($user_id);
        return parent::listResponse($order->item_list);
    }

    /**
     * Update quantity of item from cart
     * @param Request $request
     * @param array ...$id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, ...$id)
    {
        $user_id = $id[1];
        $product_id = $id[0];
        $this->validateUser($user_id);
        $this->validate($request, $this->getValidationRules());

        $order    = Order::getLastOrder($user_id);
        $product  = Product::findOrFail($product_id);
        $quantity = $request->get('quantity') ?: 0;

        if (!$order->items->contains($product)) {
            throw new ModelNotFoundException("You haven't bought this product before");
        }

        if ($product->quantity < $quantity ) {
            throw new RuntimeException("Insufficient product quantity");
        }

        $order->items()->detach($product);
        $order->items()->attach($product,['quantity'=>$quantity]);

        return parent::createdResponse(Order::getLastOrder($id[1])->item_list);
    }

    /**
     * Remove item from cart
     * @param array ...$id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(...$id)
    {
        $user_id = $id[1];
        $product_id = $id[0];
        $this->validateUser($user_id);

        $order = Order::getLastOrder($user_id);
        $product = Product::findOrFail($product_id);
        $order->items()->detach($product);
        return parent::deletedResponse();
    }

    public function show(...$id)
    {
        throw new MethodNotAllowedHttpException([]);
    }
}