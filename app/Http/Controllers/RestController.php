<?php

namespace App\Http\Controllers;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * Class RestController
 * @package App\Http\Controllers
 */
abstract class RestController extends BaseController
{
    /**
     * get model namespace
     * @return string
     */
    public abstract function getModel();


    /**
     * get validation rules
     * @return array
     */
    public abstract function getValidationRules();

    public function getStoreValidationRules()
    {
        $rules = $this->getValidationRules();
        array_walk($rules, function (&$rule) {
            $rule .= '|required';
        });
        return $rules;
    }


    /**
     * Get list of all object on resources
     * @param Request $request
     * @param array ...$id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, ...$id)
    {
        $m = $this->getModel();
        return $this->listResponse($m::all());
    }

    /**
     * Get specific detail of resources
     * @param array ...$id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(...$id)
    {
        $m = $this->getModel();
        $data = $m::findOrFail($id)[0];
        return $this->showResponse($data);
    }

    /**
     * Store object on resource
     * @param Request $request
     * @param array ...$id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, ...$id)
    {
        $m = $this->getModel();
        $this->validate($request, $this->getStoreValidationRules());
        $data = $m::create($request->all());
        return $this->createdResponse($data);
    }

    /**
     * Update specific object of resources
     * @param Request $request
     * @param array ...$id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, ...$id)
    {
        $m = $this->getModel();
        $data = $m::findOrFail($id)[0];
        $this->validate($request, $this->getValidationRules());
        $data->fill($request->all());
        $data->save();

        return $this->showResponse($data);
    }

    /**
     * Delete object from resources
     * @param array ...$id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(...$id)
    {
        $m = $this->getModel();
        $data = $m::findOrFail($id)[0];
        $data->delete();
        return $this->deletedResponse();
    }

    /**
     * @param $id
     * @throws AuthenticationException
     */
    public function validateUser($id) {
        if (\Auth::id() != $id) {
            throw new AuthenticationException("Unauthorized access to resource");
        }
    }

    /**
     * @param $data
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createdResponse($data)
    {
        $response = [
            'code' => 201,
            'status' => 'succcess',
            'data' => $data
        ];
        return response()->json($response, $response['code']);
    }

    /**
     * @param $data
     * @return \Illuminate\Http\JsonResponse
     */
    protected function showResponse($data)
    {
        $response = [
            'code' => 200,
            'status' => 'succcess',
            'data' => $data
        ];
        return response()->json($response, $response['code']);
    }

    /**
     * @param $data
     * @return \Illuminate\Http\JsonResponse
     */
    protected function listResponse($data)
    {
        $response = [
            'code' => 200,
            'status' => 'succcess',
            'data' => $data
        ];
        return response()->json($response, $response['code']);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    protected function deletedResponse()
    {
        $response = [
            'code' => 200,
            'status' => 'success',
            'data' => [],
            'message' => 'Resource deleted'
        ];
        return response()->json($response, $response['code']);
    }
}
