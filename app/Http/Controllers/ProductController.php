<?php
/**
 * Created by PhpStorm.
 * User: zmajdy
 * Date: 10/7/2016
 * Time: 6:58 PM
 */

namespace App\Http\Controllers;


use App\User;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;

class ProductController extends RestController
{
    /**
     * get model namespace
     * @return string
     */
    public function getModel()
    {
        return 'App\Product';
    }

    /**
     * get validation rules
     * @return array
     */
    public function getValidationRules()
    {
        return [
            'quantity' => 'required|integer',
            'name' => 'required|string',
            'price' => 'required|integer'
        ];
    }

    public function store(Request $request, ...$id)
    {
        if (\Auth::user()->role != User::USER_ADMIN_ROLE) throw new AuthenticationException();
        return parent::store($request, $id);
    }

    public function update(Request $request, ...$id)
    {
        if (\Auth::user()->role != User::USER_ADMIN_ROLE) throw new AuthenticationException();
        return parent::update($request, $id);
    }

    public function destroy(...$id)
    {
        if (\Auth::user()->role != User::USER_ADMIN_ROLE) throw new AuthenticationException();
        return parent::destroy($id);
    }
}