<?php
/**
 * Created by PhpStorm.
 * User: zmajdy
 * Date: 10/8/2016
 * Time: 3:14 PM
 */

namespace App\Http\Controllers;

use App\Order;
use App\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class OrderController extends RestController
{

    /**
     * get model namespace
     * @return string
     */
    public function getModel()
    {
        return 'App\Order';
    }

    /**
     * get validation rules
     * @return array
     */
    public function getValidationRules()
    {
        return ['status'=>'in:'.implode(",",[
            Order::STATUS_SHIPPED,
            Order::STATUS_CONFIRMED,
            Order::STATUS_PENDING,
            Order::STATUS_CANCELED
        ])];
    }

    public function store(Request $request, ...$id)
    {
        throw new MethodNotAllowedHttpException([]);
    }

    public function destroy(...$id)
    {
        throw new MethodNotAllowedHttpException([]);
    }

    /**
     * Update order status by admin
     * @param Request $request
     * @param array ...$id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, ...$id)
    {
        $this->validate($request,$this->getStoreValidationRules());

        $user_id  = $id[1];
        $order_id = $id[0];
        $status   = $request->get('status');

        $user = User::find($user_id);
        $order = $user->orders()->findOrFail($order_id);
        $order->status = $status;
        $order->save();

        return $this->showResponse($order);
    }

    /**
     * get list of all orders (filter by status)
     * @param Request $request
     * @param array ...$id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, ...$id)
    {
        $orders = Order::all();
        if ($request->get('status')) {
            $this->validate($request,$this->getValidationRules());
            $orders = $orders->where('status',$request->get('status'));
        }
        return $this->listResponse($orders);
    }
}