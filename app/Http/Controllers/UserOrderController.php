<?php
/**
 * Created by PhpStorm.
 * User: zmajdy
 * Date: 10/8/2016
 * Time: 3:14 PM
 */

namespace App\Http\Controllers;

use App\Coupon;
use App\Order;
use App\Product;
use App\Shipping;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class UserOrderController extends RestController
{

    /**
     * get model namespace
     * @return string
     */
    public function getModel()
    {
        return 'App\Order';
    }

    /**
     * get validation rules
     * @return array
     */
    public function getValidationRules()
    {
        return [
            'customer_name' => 'string',
            'email' => 'email',
            'phone' => 'digits_between:6,12',
            'payment_proof' => 'string',
            'partner_name' => 'string'
        ];
    }

    /**
     * Finalize order
     * @param Request $request
     * @param array ...$id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, ...$id)
    {
        $user_id = $id[0];
        $this->validateUser($user_id);
        $order = Order::getLastOrder($user_id);

        $old   = $request->all();
        $request->request->add($order->toArray());
        $request->request->add($old);
        $this->validate($request,$this->getStoreValidationRules());

        $order = \DB::transaction(function() use ($request,$user_id,$order) {
            $this->resolveShipping($order,$request);
            $this->resolveCoupon($order,$request);
            $this->resolveItems($order);
            $order->fill($request->all());
            $order->status = Order::STATUS_PENDING;
            $order->save();
            return $order;
        });

        return $this->showResponse($order);
    }

    /**
     * Show all orders from specific user
     * @param Request $request
     * @param array ...$id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request,...$id)
    {
        $user_id = $id[0];
        $this->validateUser($user_id);

        $user = User::find($user_id);
        return $this->listResponse($user->orders);
    }

    /**
     * Show order detail
     * @param array ...$id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(...$id)
    {
        $user_id = $id[1];
        $this->validateUser($user_id);

        $order_id = $id[0];
        $user = User::find($user_id);
        return $this->listResponse($user->orders()->findOrFail($order_id));
    }

    public function destroy(...$id)
    {
        throw new MethodNotAllowedHttpException([]);
    }

    /**
     * Update order detail
     * @param Request $request
     * @param array ...$id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, ...$id)
    {
        $user_id = $id[1];
        $order_id = $id[0];

        $this->validate($request,$this->getValidationRules());
        $this->validateUser($user_id);

        $order = Order::getLastOrder($user_id);

        if ($order->id != $order_id) {
            throw new ModelNotFoundException("Order not found or already finalized");
        }

        $order = \DB::transaction(function() use ($request,$user_id,$order) {
            $this->resolveShipping($order,$request);
            $this->resolveCoupon($order,$request);
            $order->fill($request->all());
            $order->save();
            return $order;
        });

        return $this->showResponse($order);
    }

    private function resolveCoupon(Order &$order,Request $request) {
        $coupon_id = $request->get('coupon_id');
        if ($coupon_id) {
            $coupon = Coupon::whereUuid($coupon_id)->firstOrFail();
            if ($coupon->order) throw new ModelNotFoundException('This coupon has already used');
            if (strtotime($coupon->end_valid_at) < time() || time() <  strtotime($coupon->start_valid_at)) {
                throw new ModelNotFoundException("Your coupon has already expired or has not valid yet");
            }
            $order->coupon()->associate($coupon);
        }
    }

    private function resolveShipping(Order &$order,Request $request) {
        $shipping = Shipping::create($request->all());
        $order->shipping()->associate($shipping);
    }

    private function resolveItems(Order &$order) {
        foreach ($order->items() as $item) {
            $product = Product::findOrFail($item->id);
            if ($product->quantity < $item->pivot->quantity) {
                throw new ModelNotFoundException('Insufficient quantity for item : '.$product->name);
            }
            else{
                $product->quantity -= $item->pivot->quantity;
                $product->save();
            }
        }
    }
}