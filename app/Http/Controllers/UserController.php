<?php
/**
 * Created by PhpStorm.
 * User: zmajdy
 * Date: 10/8/2016
 * Time: 2:49 PM
 */

namespace App\Http\Controllers;


use App\User;

class UserController extends RestController
{

    /**
     * get model namespace
     * @return string
     */
    public function getModel()
    {
        return 'App\User';
    }

    /**
     * get validation rules
     * @return array
     */
    public function getValidationRules()
    {
        return [
            'email' => 'required|email',
            'password' => 'required|string',
            'role' => 'required|in:'.User::USER_ADMIN_ROLE.','.User::USER_CUSTOMER_ROLE
        ];
    }
}