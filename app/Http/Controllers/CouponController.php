<?php
/**
 * Created by PhpStorm.
 * User: zmajdy
 * Date: 10/8/2016
 * Time: 11:41 AM
 */

namespace App\Http\Controllers;


use App\Coupon;

class CouponController extends RestController
{
    /**
     * get model namespace
     * @return string
     */
    public function getModel()
    {
        return 'App\Coupon';
    }

    /**
     * get validation rules
     * @return array
     */
    public function getValidationRules()
    {
        return [
            'quantity' => 'integer',
            'name' => 'string',
            'discount' => 'integer',
            'discount_type' => 'in:' . Coupon::PERCENT_TYPE_COUPON . ',' . Coupon::NOMINAL_TYPE_COUPON,
            'start_valid_at' => 'integer',
            'end_valid_at' => 'integer'
        ];
    }
}
