<?php
/**
 * Created by PhpStorm.
 * User: zmajdy
 * Date: 10/7/2016
 * Time: 4:03 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Shipping
 * @package App\Providers
 *
 * @property string uuid
 * @property string status
 * @property string partner_name
 */
class Shipping extends Model
{
    const SHIPPED_STATUS = 'shipped';
    const DELIVERED_STATUS = 'delivered';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'partner_name',
        'status'
    ];

    protected $hidden = ['created_at', 'updated_at', 'id'];

    public function __construct(array $attributes = [])
    {
        $this->uuid = uniqid();
        $this->status = self::SHIPPED_STATUS;
        parent::__construct($attributes);
    }
}