<?php
/**
 * Created by PhpStorm.
 * User: zmajdy
 * Date: 10/7/2016
 * Time: 3:57 PM
 */
namespace App;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Product
 *
 * @property integer $price
 * @property integer $quantity
 * @property string $name
 */
class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'quantity', 'price'
    ];

    public function orders() {
        return $this->belongsToMany('App\Order')->withPivot('quantity');
    }
}
