# Order-Transaction API

Order-Transaction API for Salestock assessment. 
Built using Lumen REST Framework. 
You can access the live host at [here](http://104.236.76.161/api/v1)

## Running Locally
```
docker run --net=host zmajdy/order-transaction bash /home/order-transaction/start_service.sh
```

## Documentation

###Base Host : /api/v1

###Authorization

Every request (Except ```POST /users/{userid}/tokens```) should be beared by authorization token header :
```
Authorization : Bearer YOUR_TOKEN_HERE
```
You can get the API token by hitting this endpoint 
```
POST /users/{user_id}/tokens 
```

Hardcoded account :

Admin
```
POST /api/v1/users/1/tokens
{
    "email" : "admin@admin.com",
    "password" : "admin"
}
```

Customer
```
POST /api/v1/users/2/tokens
{
    "email" : "customer@customer.com",
    "password" : "customer"
}
```


### Endpoints :

All endpoint followed by * means the request should be beared by authorization token of user with admin role. 

####Product Endpoint
#####Endpoints:
```
    GET /products 
	    Get All products
	POST /products
   	    Add new product
	GET /procuts/{product_id}*
	    Get product by ID
	DELETE /procuts/{product_id}*
	    Delete product
	PUT /procuts/{product_id}*
	    Update product
```
#####Create/Update Request:
```
    {
        "name": string,
        "quantity": integer,
        "price": integer,
    }
```

####Coupon Endpoint	  
#####Endpoints:
```
	GET /coupons*
        Get All coupons
    POST /coupons*
        Add new coupon
    GET /coupons/{coupon_id}*
        Get coupon by ID
    DELETE /coupons/{coupon_id}*
        Delete coupon
    PUT /coupon/{coupon_id}*
        Update coupon
```
#####Create/Update Request
```
    {
      "quantity": integer,
      "discount": integer,
      "discount_type": string('nominal'/'percent'),
      "start_valid_at": integer(timestamp),
      "end_valid_at": integer(timestamp),
    }
```

####Order Endpoint
#####Endpoints:
```
	GET /orders?status={status}*
        Get All order, filter by status
    PUT /orders/{order_id}*
        Update order status by admin (shipped/confirmed)
```
#####Create/Update Request
```
    {
      "status" : string(shipped|confirmed|pending|canceled)
    }
```


####User Endpoint
#####Endpoints:
```
	GET /users*
        Get All users
    POST /users*
        Add new users
    GET /users/{user_id}*
        Get user by ID
    DELETE /users/{user_id}*
        Delete user
    PUT /procuts/{product_id}*
        Update user
```
#####Create/Update Request
```
    {
      "email": string,
      "role": string,
      "password" : string
    }
```

####User Token Endpoint
####Endpoints:
```
	GET /users/{user_id}/tokens
        Get All user tokens
    POST /users/{user_id}/tokens
        Register new token (get authorization token)
    GET /users/{user_id}/tokens/{token_id}
        Get user token by id
    DELETE /users/{user_id}
        Delete user token (revoke access)
```
#####Create/Update Request
```
    {
      "email": string,
      "password" : string
    }
```


####User Cart Endpoint
#####Endpoints:
```
    GET /users/{user_id}/cart
        Get All item on cart (item list)
    POST /users/{user_id}/cart
        Add new item to cart
    Delete /users/{user_id}/cart/{product_id}
        remove item from cart
    PUT /users/{user_id}/cart/{product_id}
        update item from cart quantity
```
#####Create/Update Request
```
    {
        "product_id" : integer,
        "quantity" : integer
    }
```

####User Order Endpoint
#####Endpoints:
```
	GET /users/{user_id}/orders
        Get order history of user
    POST /users/{user_id}/orders
        Finalize (post order data) of currently active order (items from cart)
        Coupon can be submitted while finalizing order
    PUT /users/{user_id}/orders
        Update order data (payment proof and bio)
        Coupon can be submitted while updating order data
    GET /users/{user_id}/orders/{token_id}
        Get user token by id
```
#####Create/Update Request
```
    {
        "coupon_id": string(uuid)(not required)
        "customer_name" : string,
        "phone": string(digit only),
        "email": string,
        "payment_proof": string,
        "partner_name" : string
    }
```

## License

licensed under the [MIT license](http://opensource.org/licenses/MIT)