<?php
use Illuminate\Database\Seeder;
use App\Coupon;

/**
 * Created by PhpStorm.
 * User: zmajdy
 * Date: 10/7/2016
 * Time: 5:34 PM
 */


class CouponTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i =1; $i<=10; $i++) {
            $coupon                   = new Coupon();
            $coupon->start_valid_at   = time();
            $coupon->end_valid_at     = time() + ($i%3==0 ? 0 : 365*24*60*60);
            $coupon->quantity         = $i*2-$i/2;
            $coupon->discount         = 100-$i*3;
            $coupon->discount_type    = $i%2==0 ? Coupon::PERCENT_TYPE_COUPON : Coupon::NOMINAL_TYPE_COUPON;
            $coupon->name             = 'Coupon-'.$i;
            $coupon->save();
        }
    }
}