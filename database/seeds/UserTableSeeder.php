<?php
use App\User;
use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: zmajdy
 * Date: 10/7/2016
 * Time: 5:51 PM
 */
class AdminTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new User();
        $admin->email    = 'admin@admin.com';
        $admin->password = 'admin';
        $admin->role     = User::USER_ADMIN_ROLE;
        $admin->save();

        $admin = new User();
        $admin->email    = 'customer@customer.com';
        $admin->password = 'customer';
        $admin->role     = User::USER_CUSTOMER_ROLE;
        $admin->save();
    }
}