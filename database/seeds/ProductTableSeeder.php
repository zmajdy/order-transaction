<?php
/**
 * Created by PhpStorm.
 * User: zmajdy
 * Date: 10/7/2016
 * Time: 4:40 PM
 */

use Illuminate\Database\Seeder;
use App\Product;

class ProductTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i<=10; $i++) {
            $product = new Product();
            $product->name      = "Product-".$i;
            $product->price     = $i*1000 + 500*($i%2);
            $product->quantity  = $i*2-$i/2;
            $product->save();
        }
    }
}

