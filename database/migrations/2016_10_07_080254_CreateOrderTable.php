<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function(Blueprint $table)
        {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->integer('total_price');
            $table->uuid('coupon_id')->nullable();
            $table->uuid('shipping_id')->nullable();
            $table->string('status');
            $table->string('customer_name');
            $table->string('phone');
            $table->string('email');
            $table->string('payment_proof');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('coupon_id')->references('uuid')->on('coupons');
            $table->foreign('shipping_id')->references('uuid')->on('shippings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
